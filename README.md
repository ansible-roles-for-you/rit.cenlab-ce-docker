rit.cen.gitlab-ce-docker
========================

Ensure that a GitLab CE docker container is running and is responding to HTTP requests.

Requirements
------------

The following requirements **must** be met before using this role:

- The Docker engine must be installed and running on the target host

- The docker-py python module must be installed on the target host

- Network connection to https://hub.docker.com must be available from the target host

Role Variables
--------------

This role uses a single dictionary object named *rit_cen_gitlab_ce_docker* as the role variable in order to configure the docker container.

If the variable *rit_cen_gitlab_ce_docker* is empty, the following defaults will be used:

    
    rit_cen_gitlab_ce_docker:
      container:
        name: gitlab
        hostname: gitlab
        imageversion: latest
        networkname: gitlabnetwork
        volume_config: /srv/gitlab/config
        volume_log: /srv/gitlab/log
        volume_data: /srv/gitlab/data
        port_https: 8443
        port_http: 8080
        port_ssh: 222
        env:
          GITLAB_ROOT_PASSWORD: password
          GITLAB_OMNIBUS_CONFIG: omnibus_config_parameters


Each member of the dictionary can be individually overridden.

Dependencies
------------

This role does not pull in any automatic dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: centos
      roles:
         - { role: rit.cen.gitlab-docker-ce }

License
-------

BSD

Author Information
------------------

Contact http://www.regal-it.com.au/
